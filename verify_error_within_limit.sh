#!/bin/sh

NUM_TRANS=$(grep -nr "Summary" jmeter.log | grep -o '[^ ]*' | tail -14 | head -1)
NUM_ERRORS=$(grep -nr "Summary" jmeter.log | grep -o '[^ ]*' | tail -2 | head -1)
AVERAGE_RESPONSE=$(grep -nr "Summary" jmeter.log | grep -o '[^ ]*' | tail -8 | head -1)
ERROR_RATE=$NUM_ERRORS * 100 / $NUM_TRANS
echo "NUM_TRANS = $NUM_TRANS"
echo "NUM_ERRORS = $NUM_ERRORS"
echo "ERROR_RATE = $ERROR_RATE"

echo "AVERAGE_RESPONSE = $AVERAGE_RESPONSE"

ret_val = success
if [ $ERROR_RATE -gt $1 ]
then
   echo "ERROR rate too high. Max allowed =" $1", actual rate =" $ERROR_RATE
   ret_val = failed
else
   echo "Error rate within limits. Max allowed =" $1", actual rate =" $ERROR_RATE
fi

if [ $AVERAGE_RESPONSE -gt $2 ]
then
   echo "Average response time (ms) too high. Max allowed = " $2", actual rate = " $AVERAGE_RESPONSE
   ret_val = failed
else
   echo "Average response time (ms) within limits. Max allowed = " $2", actual rate = " $AVERAGE_RESPONSE
fi

if [[ "$ret_val" == success ]]; then
   exit 0
else
   exit 1
fi