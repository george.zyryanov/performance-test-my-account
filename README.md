# Serverside performance: JMeter

This image runs jmeter performance tests. It is based on the 
`docker.io/justb4/jmeter` image.

## Gitlab CI

Adding execution to your pipeline is as easy as copy-paste below snippet and adjusting
the URL argument and the JMeter script. It will run a JMeter test as defined in the jmx file, and 
parse the outputs to ensure that the average time and the average error rates are below configured thresholds.  
The job will fail if these values are exceeded.

```yaml
include:
  - project: 'rs-devsecops/rs-dev/ci-build/jmeter'
    ref: 0.2-buttons
    file: '/ci-templates/.gitlab-ci-template-jmeter.yml'
    
test:jmeter:happy-path:
  stage: test
  extends: .jmeter:serverside-performance
  variables:
    JMETER_CONFIG_FILE: "test/my-account-service-test.jmx"
    JMETER_TEST_DOMAIN: "https://st1-ie.rs-online.com/web/"
```

Once the job has ran, gitlab will save the report as artifacts and based on the sample configuration
above, it will keep it for 30 days.


By default the results are pushed to DataDog and you will also see a link to DataDog dashboard with JMeter test results for your run. 
Copy/paste into the url bar to view.


### Important: DATADOG Credentials

Your Gitlab CI project needs to have access to read `secret/shared/datadog-pipeline` in Vault. By default
all projects without specific vault tokens have access to that path, but if you're using your own token,
make sure the policy has been updated to add access to the above mentioned path. If you need to do that,
raise a CCC ticket in Jira at https://rs-components.atlassian.net/browse/CCC.

### Customisation

There are a few things you can customise. 

*JMETER_TEST_DOMAIN*

This is the Domain that is to be used to run the tests against
######Default value: general-display-i18n-dev.services.nonprod.rscomp.systems

*JMETER_CONFIG_FILE*

This is the JMeter configuration file used for the test. It should in the project under test.
######Default value: "test/ramp.jmx"

*JMETER_MAX_ALLOWABLE_ERR_RATE_PERCENT*

The maximum allowable percentage error rate
######Default value: 5

*JMETER_MAX_ALLOWABLE_AVG_RESPONSE_TIME_MS*

The maximum allowable average response rate.
######Default value: 500

*JMETER_EXTRA_PARAMETERS*

Additional configuration parameters to add to the jmeter CLI command in addition to the test domain and config file.
All runs will use '-n' - Non GUI and '-t' for the test file location.
######Default value: '-l results.jtl'
Post-processing relies on the jmeter run creating a results.jtl file (either by inclusion in this parameter, or from the jmeter project)

*PUSH_TO_DATADOG*
You can stop the push of results to datadog (i.e. if the results are large) by setting the variable PUSH_TO_DATADOG 
######Default value: 'true'


