FROM docker.io/justb4/jmeter@sha256:343e8700dbb1c08fa4f4fd7dd099a17570bf45a17a63578eb31b8f35d20c61eb
# justb4/jmeter:3.3

ENV JVM_ARGS="-Xmn64m -Xms256m -Xmx256m"
ENV VAULT_CLI_VERSION=1.1.3

RUN    apk update \
	&& apk upgrade \
	&& apk add ca-certificates \
	&& update-ca-certificates \
	&& apk add --update curl unzip \
	&& apk add --no-cache nss \
	&& rm -rf /var/cache/apk/* \
	&& mkdir -p /tmp/dependencies
       
RUN touch ${JMETER_HOME}/jmeter.log && chmod 666 ${JMETER_HOME}/jmeter.log
COPY entrypoint.sh /
COPY verify_error_within_limit.sh /usr/bin/verify_error_within_limit.sh
COPY *.py  /usr/bin/
RUN chmod 755 /entrypoint.sh /usr/bin/verify_error_within_limit.sh /usr/bin/pass_fail_jmeter_summary.py

COPY lib/*.jar ${JMETER_HOME}/lib/
COPY lib/ext/*.jar ${JMETER_HOME}/lib/ext/

RUN adduser -h /home/jmeter -s /sbin/nologin -u 1000 -D jmeter
RUN apk add --no-cache jq python && \
    python -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip install --upgrade pip setuptools && \
    pip install datadog && \
    curl -LO https://releases.hashicorp.com/vault/${VAULT_CLI_VERSION}/vault_${VAULT_CLI_VERSION}_linux_amd64.zip  && \
    unzip vault_${VAULT_CLI_VERSION}_linux_amd64.zip && \
    mv vault /usr/local/bin && \
    rm -r /root/.cache
RUN apk --update add \
       fontconfig \
       ttf-dejavu
WORKDIR /home/jmeter
USER jmeter
