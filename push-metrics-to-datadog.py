#!/usr/bin/env python

import ast
import csv
import os
import re
import sys

from datadog import api, initialize, statsd

EXCLUDED_FIELDS = [ 'responsemessage', 'threadname' ]

options = {
    'statsd_host': '172.17.0.1',
    'statsd_port': '8125',
    'mute': False
}

SEND_VIA_API=os.getenv('DATADOG_API_KEY') != None

initialize(**options)

metrics = []

def get_current_metric(field, timestamp, value):
    metric = {}
    metric.update({'metric': 'jmeter.transaction.' + field})
    metric.update({'points': [(timestamp, value)] })
    
    curtags = {}
    curtags.update({'committer_name': os.getenv('GITLAB_USER_EMAIL') })
    curtags.update({'job_name': os.getenv('CI_JOB_NAME') })
    curtags.update({'project_name': os.getenv('CI_PROJECT_NAME') })
    curtags.update({'ref': os.getenv('CI_COMMIT_REF_NAME') })
    curtags.update({'gitlab_job_id': os.getenv('CI_JOB_ID') })
    curtags.update({'top_level_group': 'rs-dev' })
    curtags.update({'env': 'pipeline' })
    curtags.update({'source': 'gitlab' })

    metric.update({'tags': curtags})
    return metric


jtl_file = sys.argv[1]

jtl_file = open(sys.argv[1])
csv_file = csv.DictReader(jtl_file)

def underscore(string):
  if string == "":
    return None

  return string[0].lower() + re.sub(r"([A-Z])", "_\\1", string[1:]).lower()

def get_value_from_string(string):
  if string == "":
    return None
  
  try:
    return ast.literal_eval(string)
  except:
    return string

for row in csv_file:
  timestamp = int(row['timeStamp'])/1000

  labels = {}
  row_metrics = []

  for field_name in csv_file.fieldnames:
    underscore_field_name = underscore(field_name)
    field_value = get_value_from_string(row[field_name])

    if not isinstance(field_value, str) and field_value != None:
      metric = get_current_metric(underscore_field_name, timestamp, field_value)
      row_metrics.append(metric)
      metrics.append(metric)
    elif EXCLUDED_FIELDS.count(field_name.lower()) == 0:
      labels.update({ underscore_field_name: field_value})
  
  for k in labels:
    for row_metric in row_metrics:
      row_metric['tags'].update({k:labels[k]})

if SEND_VIA_API:
  print "Sending metrics to datadog (API)"
  api.Metric.send(metrics)
else:
  print "Sending metrics to datadog (StatsD)"
  for metric in metrics:
    tags = []
    for tag in metric['tags']:
      tags.append(tag + ":" + str(metric['tags'][tag]))

    statsd.gauge(metric['metric'], metric['points'][0][1], tags=tags)
