import sys
import csv
if len(sys.argv) <> 4:  # Check correct number of arguments
    print "Invalid number of arguments. Need summary_file_name max_allowable_average_response_ms max_allowable_error_rate"
    exit(1)
f = open(sys.argv[1])
csv_f = csv.reader(f)
firstline = True
failureFound = False
print "Output summary file to compare against = " + sys.argv[1]
print "Max allowable average response rate = " + sys.argv[2] + "ms"
print "Max allowable error rate = " + sys.argv[3] + "%"
for row in csv_f:
  if firstline:    #skip first line
    firstline = False
    continue
  averageTime = row[2]
  errorPercent = row[7]
  errorPercent = errorPercent[:-4]   # Format is always XX.nn% so revert to integer percentage
  print "Average time for row = " + averageTime + ", for group " + row[0]
  print "Percentage error for row = " +errorPercent + ", for group " + row[0]
  # Check if average time is greater than allowed value
  if int(averageTime) > int(sys.argv[2]):
    print "ERROR average time of response is too high. Average is " + averageTime + ", max allowed is " + sys.argv[2] + ", for group " + row[0]
    failureFound = True

  # Check if error percentage is greater than allowed value
  if int(errorPercent) > int(sys.argv[3]):
    print "ERROR error rate is too high. Average is " + errorPercent + ", max allowed is " + sys.argv[3] + ", for group " + row[0]
    failureFound = True

if failureFound == True:
  exit(1)
else:
  exit(0)